﻿// See https://aka.ms/new-console-template for more information
/*Создайте новый проект или откройте предыдущий, объявите несколько переменных различных
    типов, примените явное и неявное преобразование. Создайте константную переменную,
    попробуйте изменить ее значение.*/
int i1 = 15;
string i2 = "User";
double i3 = 1.7;
Console.WriteLine(i1);
Console.WriteLine(i2);
Console.WriteLine(i3);
i1 = (int)i3;
Console.WriteLine(i1);
const uint i4 = 54;
/*i4 = 55;*/
Console.WriteLine(i4);
var i5 = (int)i4;
Console.WriteLine(i5);
/*const var i6 = "Name";*/